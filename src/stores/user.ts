import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/Users";
import userSerVices from "@/services/users";

export const useUserStore = defineStore("user", () => {
  const Users = ref<User[]>([]);
  const editedUser = ref<User>({ name: "", tel: "", age: 0, gender: "" });
  async function getUser() {
    try {
      const res = await userSerVices.getUsers();
      Users.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }
  return { editedUser, getUser, Users };
});
