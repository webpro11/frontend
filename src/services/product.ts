import type Product from "@/types/Product";
import http from "./axios";
function getProduct() {
  return http.get("/products");
}

function saveProduct(product: Product) {
  return http.post("/products", product);
}
function UpdateProduct(id: number, product: Product) {
  return http.patch(`/products/${id}`, product);
}
function DeleteProduct(id: number) {
  return http.delete(`/products/${id}`);
}
export default { getProduct, saveProduct, UpdateProduct, DeleteProduct };
